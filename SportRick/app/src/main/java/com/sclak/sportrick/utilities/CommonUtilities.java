package com.sclak.sportrick.utilities;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.sclak.sportrick.R;
import com.sclak.sportrick.dialogs.SclakDialog;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by francesco on 22/05/15.
 */
public class CommonUtilities {

    private static final String TAG = CommonUtilities.class.getName();
    private static Handler handler = new Handler();

    public static boolean isApplicationBroughtToBackground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }

        return false;
    }

    public static void sendAlert(final String title, final String message, final Context context, final View.OnClickListener listener) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                final SclakDialog dialog = new SclakDialog(context);
                dialog.setTitle(title);
                dialog.setMessage(message);
                dialog.setCancelable(false);

                dialog.setPositiveButton(context.getString(R.string.ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (listener != null) {
                            listener.onClick(v);
                        }
                    }
                });

                dialog.show();
            }
        });
    }

    private static void changeButtonStatus(final Resources resources, final Button b,
                                           final boolean fullButton, boolean statusOk, boolean forceDisable,
                                           final List<Integer> drawableList,
                                           int enabledFullBtn, int enabledBorderBtn, int disabledBorderBtn,
                                           int enabledFullBtnTextColor, int enabledBorderBtnTextColor, int disabledBorderBtnTextColor,
                                           final int colorFull, final int colorEmpty) {
        //set background color style
        Drawable buttonEnabledDrawable = fullButton ? resources.getDrawable(enabledFullBtn) : resources.getDrawable(enabledBorderBtn);
        b.setBackground(statusOk ? buttonEnabledDrawable : resources.getDrawable(disabledBorderBtn));

        //set text color
        if (statusOk) {
            b.setTextColor(fullButton ? resources.getColorStateList(enabledFullBtnTextColor) : resources.getColorStateList(enabledBorderBtnTextColor));
        } else {
            b.setTextColor(resources.getColor(disabledBorderBtnTextColor));
        }

        //set left drawable if set
        if (drawableList != null && drawableList.size() == 2 && !forceDisable && statusOk) {
            //set active icon
            b.setCompoundDrawablesWithIntrinsicBounds(resources.getDrawable(drawableList.get(fullButton ? 1 : 0)), null, null, null);

            b.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    int maskedAction = motionEvent.getActionMasked();
                    if (maskedAction == MotionEvent.ACTION_DOWN) {
                        b.setCompoundDrawablesWithIntrinsicBounds(resources.getDrawable(drawableList.get(fullButton ? 0 : 1)), null, null, null);
                        b.setTextColor(fullButton ? resources.getColor(colorFull) : resources.getColor(colorEmpty));
                    } else if (maskedAction == MotionEvent.ACTION_UP) {
                        b.setCompoundDrawablesWithIntrinsicBounds(resources.getDrawable(drawableList.get(fullButton ? 1 : 0)), null, null, null);
                        b.setTextColor(fullButton ? resources.getColor(colorEmpty) : resources.getColor(colorFull));
                    }
                    return false;
                }
            });
        } else if (drawableList != null && drawableList.size() == 2 && forceDisable && statusOk) {
            //set disable active icon
            b.setCompoundDrawablesWithIntrinsicBounds(resources.getDrawable(drawableList.get(0)), null, null, null);
        }

        b.setEnabled(statusOk);
        b.setClickable(statusOk);
        if (forceDisable) {
            b.setClickable(false);
            b.setOnTouchListener(null);
        }
    }

    public static void changeButtonStatus(final Resources resources, final Button b, final boolean fullButton, boolean statusOk, boolean forceDisable, final List<Integer> drawableList) {
        changeButtonStatus(resources, b, fullButton, statusOk, forceDisable, drawableList,
                R.drawable.button_rectangular_blue_full,
                R.drawable.button_rectangular_blue_border,
                R.drawable.button_rectangular_empty_gray,
                R.color.light_gray_blue,
                R.color.blue_light_gray,
                R.color.disabled_gray,
                R.color.blue,
                R.color.light_gray);
    }



    public static void showKeyBoard(Activity activity) {
        if (activity == null) {
            Log.w(TAG, "activity is null, cannot show keyboard");
            return;
        }
        InputMethodManager mgr = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.showSoftInput(activity.getCurrentFocus(), InputMethodManager.SHOW_IMPLICIT);
        //mgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static void toggleKeyboard(Context context) {
        final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static void hideKeyBoard(Activity activity) {
        try {
            if (activity == null) {
                Log.d(TAG, "activity is null, cannot hide keyboard");
                return;
            }
            InputMethodManager mgr = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            mgr.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ex) {

        }
    }

    public static boolean validatePassword(String pwd) {
        // check length
        if (pwd.length() < 8) {
            Log.e(TAG, "wrong password, len is: " + pwd.length());
            return false;
        }

        return true;
/*
        // check if contains a number
        final String NUMERIC_PATTERN = "[0-9]";
        Pattern regex = Pattern.compile(NUMERIC_PATTERN);
        Matcher matcher = regex.matcher(pwd);
        if (!matcher.find()){
            Log.e(TAG, "wrong password, don't contain a number");
            return false;
        }

        // check if contains special char
        final String SPECIAL_CHAR_PATTERN = "[#@$%^&*!?]";
        regex = Pattern.compile(SPECIAL_CHAR_PATTERN);
        matcher = regex.matcher(pwd);
        if (!matcher.find()){
            Log.e(TAG, "wrong password, don't have a special char (" + SPECIAL_CHAR_PATTERN + ")");
            return false;
        }

        // check if have an upper case
        boolean foundUpperCaseChar = false;
        for (int i = 0; i < pwd.length(); i++) {
            if (Character.isUpperCase(pwd.charAt(i))) {
                foundUpperCaseChar = true;
                break;
            }
        }
        if (!foundUpperCaseChar) {
            Log.e(TAG, "wrong password, don't have an upper case letter");
            return false;
        }

        return true;*/
    }

    public static boolean validateNameOrSurname(String str) {
        return str.length() >= 3;
    }

    public static boolean validateEmail(String email) {
        Pattern pattern;
        Matcher matcher;

        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public static boolean validateTelephone(String telephone) {
        Pattern pattern;
        Matcher matcher;

        String TEL_PATTERN = "\\+?([0-9]{6,15})";
        pattern = Pattern.compile(TEL_PATTERN);
        matcher = pattern.matcher(telephone);

        return matcher.matches();
    }

    public static String cleanTelephone(String telephone) {
        Pattern pattern;
        Matcher matcher;

        String TEL_PATTERN = "([^0-9\\+]+)";
        pattern = Pattern.compile(TEL_PATTERN);
        matcher = pattern.matcher(telephone);

        return matcher.replaceAll("");
    }
}
