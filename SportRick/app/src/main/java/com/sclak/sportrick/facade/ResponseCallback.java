package com.sclak.sportrick.facade;

/**
 * Created by danielepoggi on 14/11/14.
 */
public interface ResponseCallback<T> {

    public void requestCallback(boolean success, T responseObject);

}
