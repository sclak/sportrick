package com.sclak.sportrick.facade;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sclak.sportrick.facade.generic.SCKVolleyFacade;
import com.sclak.sportrick.facade.models.Access;
import com.sclak.sportrick.facade.requests.GsonRequest;

import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by danielepoggi on 14/11/14.
 */
public class SCKFacade extends SCKVolleyFacade {

    private final static String TAG = SCKFacade.class.getSimpleName();

    private final static String url_access = "/access/pass";

    // gson
    public Gson gson = null;

    // cache
    public Access access;

    private static SCKFacade INSTANCE;

    public static SCKFacade getInstance() {
        if (null == INSTANCE) {
            INSTANCE = new SCKFacade();
        }
        return INSTANCE;
    }

    private SCKFacade() {
    }

    private void initialize() {
        String uuid = getUUID();

        // initialize gson
        initializeGson();

    }

    public void initializeGson() {
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.PRIVATE)
                .create();
    }

    public void setContext(Context context) {
        boolean doInitialize = this.context == null;
        super.setContext(context);
        if (doInitialize) {
            initialize();
        }
    }

    public void getUserAccessCallback(String tagId, final ResponseCallback<Access> callback) {
        if (null == tagId || tagId.length() == 0) {
            Log.e(TAG, "ILLEGAL ARGUMENT: tagId");
            if (null != callback) {
                callback.requestCallback(false, null);
            }
            return;
        }

        final String udid = getUUID();
        // url
        String url = API_DOMAIN + url_access;

        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        // params
        HashMap<String, String> params = new HashMap<String, String>() {{
            put("apikey", "1c6b98029ddc4995b997655a53c0937d");
            //put("deviceid", udid);
            //put("tagid", getDeviceName());
            put("deviceid", "BT100");
            put("tagid", "100");
            put("datetime", formatter.format(new Date()));
            put("batch", "0");
        }};
        String jsonParams = gson.toJson(params, HashMap.class);
        //
        url = generateGetUrl(url, params);
        // logs
        Log.d(TAG, "loginWithEmailPasswordCallback with url: " + url + " params: " + jsonParams);
        // request
        GsonRequest<Access> request = new GsonRequest<Access>(
                Request.Method.GET,
                url,
                Access.class,
                null,
                new Response.Listener<Access>() {
                    @Override
                    public void onResponse(Access response) {
                        access = response;
                        Log.d(TAG, "Allowed: " + response.Allowed);

                        if (null != callback) {
                            callback.requestCallback(true, response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "getUserAccessCallback Error", error);
                if (null != callback) {
                    callback.requestCallback(false, null);
                }
            }
        });
        addToRequestQueue(request, "getUserAccessCallback");
    }

}