package com.sclak.sportrick.activities;

import android.os.Bundle;

/**
 * Created by francesco on 21/04/14.
 */
public class ActionbarActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
