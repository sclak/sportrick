package com.sclak.sportrick.facade.models;

/**
 * Created by francesco on 22/05/15.
 */
public class CustomerData {

    public String id;
    public String firstname;
    public String lastname;
    public String gender;
    public String email;
    public String customerstate;

}
