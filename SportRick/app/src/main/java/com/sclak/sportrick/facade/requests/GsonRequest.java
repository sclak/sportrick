package com.sclak.sportrick.facade.requests;

/**
 * Created by danielepoggi on 29/06/14.
 */

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.sclak.sportrick.facade.SCKFacade;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class GsonRequest<T> extends Request<T> {

    private final static String TAG = GsonRequest.class.getSimpleName();

    private Gson mGson;
    private Class<T> clazz;
    private String requestUrl;
    private String jsonParams;
    private Listener<T> listener;
    private boolean sendJson;
    private final int timeout = 250000;

    /**
     * use this constructor if you want to send params json formatted,
     * but you want to define params overriding the getParams() method
     *
     * @param method
     * @param url
     * @param clazz
     * @param listener
     * @param errorListener
     */
    public GsonRequest(int method,
                       String url,
                       Class<T> clazz,
                       String jsonParams,
                       Listener<T> listener,
                       ErrorListener errorListener) {
        super(method, url, errorListener);
        this.requestUrl = url;
        this.clazz = clazz;
        this.listener = listener;
        this.jsonParams = jsonParams;
        this.sendJson = true;
        mGson = SCKFacade.getInstance().gson;

        setRetryPolicy(new DefaultRetryPolicy(
                timeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    public String getBodyContentType() {
        if (sendJson) {
            return "application/json;";
        } else {
            return super.getBodyContentType();
        }
    }

    /**
     * Returns the raw POST or PUT body to be sent.
     *
     * @throws AuthFailureError in the event of auth failure
     */
    @Override
    public byte[] getBody() throws AuthFailureError {
        if (!sendJson) {
            return super.getBody();
        }
        if (jsonParams == null) {
            Log.w(TAG, "jsonParams is NULL");
            return null;
        }

        return jsonParams.getBytes();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return super.getHeaders();
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return new HashMap<String, String>();
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Log.d(TAG, "" + requestUrl + " --> network response: " + json + ", class: " + clazz.getName());

            T obj = mGson.fromJson(json, clazz);
            return Response.success(obj, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }
}