package com.sclak.sportrick.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sclak.passepartout.services.BluetoothResponseCallback;
import com.sclak.passepartout.services.ibeacon.IBeacon;
import com.sclak.passepartout.utils.NotificationUtility;
import com.sclak.sportrick.R;
import com.sclak.sportrick.activities.MainActivity;
import com.sclak.sportrick.controllers.ApplicationController;
import com.sclak.sportrick.controllers.ProximityManager;
import com.sclak.sportrick.facade.ResponseCallback;
import com.sclak.sportrick.facade.SCKFacade;
import com.sclak.sportrick.facade.models.Access;
import com.sclak.sportrick.utilities.CommonUtilities;
import com.sclak.sportrick.utilities.CustomProgressDialog;
import com.sclak.sportrick.utilities.DebugUtilities;

import java.util.Date;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements ApplicationController.IApplicationCallback {

    private final static String TAG = MainActivityFragment.class.getSimpleName();

    private ApplicationController A;
    private Button sclakButton;
    private SCKFacade F = SCKFacade.getInstance();
    private TextView welcomeMessage;
    private Handler handler = new Handler();

    public MainActivityFragment() {
        A = ApplicationController.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        final ProgressDialog progress = CustomProgressDialog.init(getActivity(), getString(R.string.call_server));

        welcomeMessage = (TextView) rootView.findViewById(R.id.messageTextView);
        sclakButton = (Button) rootView.findViewById(R.id.sclakButton);
        sclakButton.setEnabled(false);
        sclakButton.setClickable(false);
        sclakButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sclakButton.setEnabled(false);
                sclakButton.setClickable(false);

                if (DebugUtilities.DEBUG_CAN_SCLAK) {
                    ((MainActivity) getActivity()).sclakActionWithBtcode(A.BT_CODE_ENABLED, new BluetoothResponseCallback() {
                        @Override
                        public void callback(boolean success, Exception exception) {
                            Log.d(TAG, "SCLAK action: " + success);
                        }
                    });
                    return;
                }
                progress.show();
                SCKFacade.getInstance().getUserAccessCallback("100", new ResponseCallback<Access>() {
                    @Override
                    public void requestCallback(boolean success, Access responseObject) {
                        sclakButton.setEnabled(true);
                        sclakButton.setClickable(true);
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }

                        if (success && responseObject.Allowed) {
                            if (responseObject.Customer != null) {
                                printWelcomeMessage(responseObject.Customer.firstname, responseObject.Customer.lastname);
                            }
                            ((MainActivity) getActivity()).sclakActionWithBtcode(A.BT_CODE_ENABLED, new BluetoothResponseCallback() {
                                @Override
                                public void callback(boolean success, Exception exception) {
                                    Log.d(TAG, "SCLAK action: " + success);
                                }
                            });
                        }
                        else {
                            Toast.makeText(getActivity(), getString(R.string.access_denied), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        CommonUtilities.changeButtonStatus(getResources(), sclakButton, true, false, false, null);
        getActivity().registerReceiver(proximityChangedReceiver, new IntentFilter(ProximityManager.PROXIMITY_CHANGED));
        A.setFragmentCallback(this);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private BroadcastReceiver proximityChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String btcode = intent.getStringExtra(ProximityManager.BTCODE_EXTRA);
            final Integer proximity = intent.getIntExtra(ProximityManager.PROXIMITY_EXTRA, IBeacon.PROXIMITY_UNKNOWN);

            if (btcode.equals(A.BT_CODE_ENABLED)) {
                Log.v(TAG, "proximityChangedReceiver... btcode: " + btcode + ", proximity: " + proximity);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CommonUtilities.changeButtonStatus(getResources(), sclakButton, true, proximity == 1, false, null);
                        if (proximity != 1) {
                            welcomeMessage.setText("");
                        }
                    }
                });
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();

        getActivity().unregisterReceiver(proximityChangedReceiver);
    }

    @Override
    public void gotValidAccess(final String firstname, final String lastname) {
        printWelcomeMessage(firstname, lastname);
    }

    private void printWelcomeMessage(final String firstname, final String lastname) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    welcomeMessage.setText(String.format(getString(R.string.welcome_message), firstname, lastname));
                }
            });
        } catch (Exception ex) {
            Log.e(TAG, "cannot set welcome message");
        }
    }
}
