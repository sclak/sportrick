package com.sclak.sportrick.facade.generic;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.sclak.sportrick.SCKApplication;
import com.sclak.sportrick.controllers.ApplicationController;

import java.util.HashMap;

/**
 * Created by danielepoggi on 14/11/14.
 */
public class SCKVolleyFacade extends SCKGenericFacade {

    private final static String TAG = SCKVolleyFacade.class.getSimpleName();

    public final static String API_DOMAIN = "https://api.sportrick.com";

    // Volley requestQueue
    private RequestQueue mRequestQueue;
    private final static String TAG_VOLLEY = "VOLLEY";

    // PUBLIC FIELDS
    public SCKApplication app;
    public ApplicationController appController;

    protected SCKVolleyFacade() {
        app = SCKApplication.getInstance();
    }

    ////////////////////////////
    // Volley APIs            //
    ////////////////////////////
    public RequestQueue getRequestQueue() throws Exception {
        if (null == context) {
            Log.e(TAG, "context not setted. remember to set context to Sclak Facade prior to making API calls");
            throw new Exception("context not setted. remember to set context to Sclak Facade prior to making API calls");
        }
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG_VOLLEY : tag);
        try {
            getRequestQueue().add(req);
        } catch (final Exception ex) {
            Log.e(TAG, "Exception", ex);
        }
    }

    public <T> void addToRequestQueue(Request<T> req) throws Exception {
        req.setTag(TAG_VOLLEY);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void start() {
        if (null == mRequestQueue) {
            return;
        }
        mRequestQueue.start();
    }

    protected String generateGetUrl(String url, HashMap<String, String> params) {
        String res = url;
        Uri uri = Uri.parse(url);
        int counter = 0;
        try {
            for (String paramKey : params.keySet()) {
                //res += counter == 0 ? "?" : "&";
                //res += paramKey + "=" + URLEncoder.encode(params.get(paramKey), "utf-8");
                uri = uri.buildUpon().appendQueryParameter(paramKey, params.get(paramKey)).build();
                counter++;
            }
            res = uri.toString();
        } catch (Exception ex) {
            Log.e(TAG, "cannot generate GET url");
        }
        return res;
    }

}