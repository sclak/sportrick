package com.sclak.sportrick.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.sclak.sportrick.R;
import com.sclak.sportrick.utilities.CommonUtilities;

/**
 * Created by francesco on 24/05/15.
 */
public class LoginActivity extends BaseActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();

    private EditText usernameEt, passwordEt;
    private Handler handler = new Handler();

    private boolean emailIsValid = false;
    private boolean passwordIsValid = false;
    private Button loginBtn;
    private ImageView emailAlert, passwordAlert;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailAlert = (ImageView) findViewById(R.id.usernameAlertImageView);
        passwordAlert = (ImageView) findViewById(R.id.passwordAlertImageView);
        usernameEt = (EditText) findViewById(R.id.usernameEditText);
        usernameEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        usernameEt.setText("francesco@toodev.com");
        usernameEt.addTextChangedListener(textWatcher);
        passwordEt = (EditText) findViewById(R.id.passwordEditText);
        passwordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        passwordEt.addTextChangedListener(textWatcher);

        loginBtn = (Button) findViewById(R.id.loginButton);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (usernameEt.getText().toString().equals("francesco@toodev.com") && passwordEt.getText().toString().equals("pignattino")) {
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                } else {
                    CommonUtilities.sendAlert(getString(R.string.alert_title_login), getString(R.string.alert_login_data_not_valid), LoginActivity.this, null);
                }
            }
        });
        validateFields();
    }

    @Override
    protected void onResume() {
        super.onResume();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                CommonUtilities.showKeyBoard(LoginActivity.this);
            }
        }, 100);
    }


    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            validateFields();
        }
    };

    private void validateFields() {
        emailIsValid = CommonUtilities.validateEmail(usernameEt.getText().toString());
        emailAlert.setVisibility(emailIsValid || usernameEt.getText().toString().isEmpty() ? View.GONE : View.VISIBLE);
        passwordIsValid = CommonUtilities.validatePassword(passwordEt.getText().toString());
        passwordAlert.setVisibility(passwordIsValid || passwordEt.getText().toString().isEmpty() ? View.GONE : View.VISIBLE);

        changeButtonStatus();
    }

    private void changeButtonStatus() {
        boolean statusOk = emailIsValid && passwordIsValid ? true : false;

        CommonUtilities.changeButtonStatus(getResources(), loginBtn, true, statusOk, false, null);
    }

}
