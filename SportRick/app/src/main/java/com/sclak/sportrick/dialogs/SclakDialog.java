package com.sclak.sportrick.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sclak.sportrick.R;

/**
 * Created by albi on 28/04/14.
 */
public class SclakDialog extends Dialog {

    private View buttonHSeparator;
    private ImageView imageView;
    private Button ok;
    private Button cancel;
    private String message;

    private TextView titleView;
    private TextView messageView;
    private EditText passwordText;
    private String title;

    private View buttonSeparator;

    private boolean showConfirmPassword = false;
    private String password;

    public SclakDialog(Context context) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog);

        ok = (Button) findViewById(R.id.dialog_ok);
        cancel = (Button) findViewById(R.id.dialog_cancel);

        titleView = (TextView) findViewById(R.id.dialog_title);
        messageView = (TextView) findViewById(R.id.dialog_message);
        passwordText = (EditText) findViewById(R.id.dialog_confirm_password);

        imageView = (ImageView) findViewById(R.id.dialog_imageView);

        buttonSeparator = findViewById(R.id.button_separator);
        buttonHSeparator = findViewById(R.id.button_h_separator);

        cancel.setVisibility(View.GONE);
        ok.setVisibility(View.GONE);

        buttonSeparator.setVisibility(View.GONE);
        imageView.setVisibility(View.GONE);
        titleView.setVisibility(View.GONE);
        messageView.setVisibility(View.GONE);
        buttonHSeparator.setVisibility(View.GONE);
        setCancelable(false);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    public SclakDialog(Context context, int theme) {
        super(context, theme);
    }

    protected SclakDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public void setShowConfirmPassword(boolean showConfirmPassword) {
        this.showConfirmPassword = showConfirmPassword;
        passwordText.setVisibility(showConfirmPassword ? View.VISIBLE : View.GONE);
    }

    public void setTitle(String title) {
        this.title = title;

        titleView.setText(this.title);

        titleView.setVisibility(View.VISIBLE);
    }

    public void setMessage(String message) {
        this.message = message;

        messageView.setText(this.message);

        messageView.setVisibility(View.VISIBLE);
    }

    public String getPassword() {
        password = passwordText.getText().toString();

        return password;
    }

    public void setPositiveButton(String ok, View.OnClickListener onClickListener) {
        this.ok.setText(ok);
        this.ok.setOnClickListener(onClickListener);

        buttonHSeparator.setVisibility(View.VISIBLE);
        this.ok.setVisibility(View.VISIBLE);

        dismiss();
    }

    public void setNegativeButton(String cancel, View.OnClickListener onClickListener) {
        this.cancel.setText(cancel);
        this.cancel.setOnClickListener(onClickListener);

        this.cancel.setVisibility(View.VISIBLE);
        buttonSeparator.setVisibility(View.VISIBLE);

        dismiss();
    }

    public void setImageView(int image_resource) {
        this.imageView.setImageResource(image_resource);
        this.imageView.setVisibility(View.VISIBLE);
    }

    private static final float ROTATE_FROM = 0.0f;
    private static final float ROTATE_TO = -10.0f * 360.0f;

    public void setShowIndicator(boolean b) {
        if (b) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(R.drawable.activity_indicator);

            RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(2000);
            rotate.setRepeatCount(Animation.INFINITE);
            rotate.setStartOffset(0);

            rotate.setInterpolator(getContext(), R.anim.linear_interpolator);

            imageView.startAnimation(rotate);
        }
    }
}
