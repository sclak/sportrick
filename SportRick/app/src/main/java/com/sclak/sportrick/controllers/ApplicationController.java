package com.sclak.sportrick.controllers;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.util.Log;

import com.sclak.passepartout.peripherals.IAuthorizedSclakAction;
import com.sclak.passepartout.utils.NotificationUtility;
import com.sclak.sportrick.SCKApplication;
import com.sclak.sportrick.facade.ResponseCallback;
import com.sclak.sportrick.facade.SCKFacade;
import com.sclak.sportrick.facade.models.Access;
import com.sclak.sportrick.utilities.DebugUtilities;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by francesco on 03/01/15.
 */
public class ApplicationController implements IAuthorizedSclakAction {

    private static final String TAG = ApplicationController.class.getSimpleName();

    private static SCKFacade F;
    private static ApplicationController INSTANCE;
    private Context context;
    private Activity activity;
    private static ToneGenerator tg;

    // DEMO BTCODE
    public static final String BT_CODE_ENABLED = "4CE2F1010113";
    public static final String BT_CODE_SECRET = "99d72fb0f83a223b6628d04f07c5ddc43b223af8b02fd799c4ddc5074fd0286607260abd4a6a3b00f757fcae7cab059a25f94d31724fb4e1c5576a630c44718c49a7610400000000";
    //public static final String BT_CODE_ENABLED = "4CE2F1010246";
    //public static final String BT_CODE_SECRET = "1b4b06ac38f18f95e4b4f953c70e706a958ff138ac064b1b6a700ec753f9b4e4ab0a73a6acaf23fd10a397391ff8ac6f2ce5cad595403f4e0a7de38231483e8f49184e04000000b4";

    public ApplicationController() {
        F = SCKFacade.getInstance();
    }

    public static ApplicationController getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ApplicationController();
            F.appController = INSTANCE;
            tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, ToneGenerator.MAX_VOLUME);
        }

        return INSTANCE;
    }

    public interface IApplicationCallback {
        public void gotValidAccess(String firstname, String lastname);
    }

    public void setFragmentCallback(IApplicationCallback fragmentCallback) {
        this.fragmentCallback = fragmentCallback;
    }

    private IApplicationCallback fragmentCallback;

    public void setContext(Context context) {
        this.context = context;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public boolean isForeground(String PackageName) {
        Context context = SCKApplication.getInstance();
        // Get the Activity Manager
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        // Get a list of running tasks
        int maxNum = 100;
        List<ActivityManager.RunningTaskInfo> task = manager.getRunningTasks(maxNum);

        // Get the info we need for comparison.
        ComponentName componentInfo = task.get(0).topActivity;

        // Check if it matches our package name.
        if (componentInfo.getPackageName().equals(PackageName)) return true;

        // If not then our app is not on the foreground.
        return false;
    }

    private void closeAllActivities(Context context) {
        if (null == context) {
            Log.e(TAG, "ILLEGAL STATE: context is null");
        }
        Intent intent = new Intent(context, context.getClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    public int getAutoopenModeAndCheckPermission(String btcode, boolean checkPermission) {
        if (btcode.equals(BT_CODE_ENABLED)) {
            if (DebugUtilities.DEBUG_CAN_SCLAK) {
                return 1;
            }
            if (checkPermission) {
                F.access = null;
                final CountDownLatch latch = new CountDownLatch(1);
                final long startTime = new Date().getTime();
                SCKFacade.getInstance().getUserAccessCallback("100", new ResponseCallback<Access>() {
                    @Override
                    public void requestCallback(boolean success, Access responseObject) {
                        NotificationUtility.sendNotification(context, "Server call execution time: " + (new Date().getTime() - startTime), TAG, this.getClass(), false, false, true, true);
                        latch.countDown();
                    }
                });

                // wait for facade to finish
                try {
                    Log.d(TAG, "waiting...");
                    latch.await(10, TimeUnit.SECONDS);
                    if (F.access != null) {
                        Log.d(TAG, "ACCESS: " + F.access.Allowed);
                        if (!F.access.Allowed) {
                            tg.startTone(ToneGenerator.TONE_PROP_BEEP);
                        } else {
                            if (fragmentCallback != null) {
                                fragmentCallback.gotValidAccess(F.access.Customer.firstname, F.access.Customer.lastname);
                            }
                        }
                        return F.access.Allowed ? 1 : 0;       //AUTO OPEN Authorized
                    } else {
                        Log.d(TAG, "CANNOT SCLAK, access NULL");
                        tg.startTone(ToneGenerator.TONE_PROP_BEEP);
                        return 0;
                    }
                } catch (InterruptedException e) {
                    Log.e(TAG, "Exception", e);
                    tg.startTone(ToneGenerator.TONE_PROP_BEEP);
                    return 0;
                }
            } else {
                if (F.access != null) {
                    return F.access.Allowed ? 1 : 0;       //AUTO OPEN Authorized
                } else {
                    return 1;       //auto open mode
                }
            }
        }

        Log.d(TAG, "CANNOT SCLAK");
        tg.startTone(ToneGenerator.TONE_PROP_BEEP);
        return 0;
    }

    @Override
    public int countKnockKnockDevices() {
        return 0;
    }

    @Override
    public int getAutoOpenProximity(String btcode) {
        if (btcode.equals(BT_CODE_ENABLED)) {
            return 1;       //IMMEDIATE
        }
        return 0;
    }

    @Override
    public int getAutocloseTime(String btcode) {
        if (btcode.equals(BT_CODE_ENABLED)) {
            return 1;
        }
        return 0;
    }

}
