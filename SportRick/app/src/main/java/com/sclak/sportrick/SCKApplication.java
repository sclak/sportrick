package com.sclak.sportrick;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;

import com.sclak.sportrick.facade.SCKFacade;
import com.sclak.sportrick.utilities.AppForeground;
import com.sclak.sportrick.utilities.ScreenReceiver;

/**
 * Created by francesco on 22/05/15.
 */
public class SCKApplication extends Application {

    private final static String TAG = SCKApplication.class.getSimpleName();

    private BroadcastReceiver screenReceiver;
    private static SCKApplication INSTANCE;

    public static SCKApplication getInstance() {
        return INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // singleton
        INSTANCE = this;

        // checker for application foreground
        AppForeground.init(INSTANCE);

        // init facade
        SCKFacade.getInstance().setContext(this);

        // Initialize Screen receiver
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        screenReceiver = new ScreenReceiver();
        registerReceiver(screenReceiver, filter);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        try {
            unregisterReceiver(screenReceiver);
        } catch (Exception ex) {
        }
    }

    public static boolean isScreenOn() {
        return ScreenReceiver.wasScreenOn;
    }

}
