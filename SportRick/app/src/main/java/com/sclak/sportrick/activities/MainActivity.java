package com.sclak.sportrick.activities;

import android.os.Bundle;

import com.sclak.passepartout.managers.SecretManager;
import com.sclak.sportrick.R;
import com.sclak.sportrick.controllers.ApplicationController;
import com.sclak.sportrick.utilities.AppForeground;
import com.sclak.sportrick.utilities.CommonUtilities;


public class MainActivity extends BluetoothActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private AppForeground.Listener foregroundListener;
    public static boolean isAppWentToBg = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set secret
        SecretManager.getInstance(this).setSecretForBtcode(ApplicationController.getInstance().BT_CODE_ENABLED, ApplicationController.getInstance().BT_CODE_SECRET);

        //app foreground listener
        foregroundListener = new AppForeground.Listener() {
            public void onBecameForeground() {
                //Toast.makeText(getApplicationContext(), "App is in Foreground", Toast.LENGTH_SHORT).show();
                isAppWentToBg = false;

                updateManagerForeground();
            }

            public void onBecameBackground() {
                if (!isAppWentToBg && CommonUtilities.isApplicationBroughtToBackground(MainActivity.this)) {
                    //Toast.makeText(getApplicationContext(), "App is going to Background", Toast.LENGTH_SHORT).show();
                    isAppWentToBg = true;

                    updateManagerBackground();
                }
            }
        };

        AppForeground.get(this).addListener(foregroundListener);
    }

    @Override
    protected void onResume() {
        super.onResume();

        startBluetooth();
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
