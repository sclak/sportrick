package com.sclak.sportrick.activities;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.sclak.passepartout.managers.SecretManager;
import com.sclak.passepartout.peripherals.PPLDiscoveredPeripheral;
import com.sclak.passepartout.peripherals.SclakPeripheral;
import com.sclak.passepartout.services.BleNotAvailableException;
import com.sclak.passepartout.services.BleScannerNotifier;
import com.sclak.passepartout.services.BluetoothResponseCallback;
import com.sclak.passepartout.services.ICentralConsumer;
import com.sclak.passepartout.services.PPLBeacon;
import com.sclak.passepartout.services.PPLCentralManager2;
import com.sclak.passepartout.services.ibeacon.IBeacon;
import com.sclak.passepartout.services.ibeacon.RangeNotifier;
import com.sclak.passepartout.services.ibeacon.Region;
import com.sclak.passepartout.services.utils.PPLCentralManagerOptions;
import com.sclak.passepartout.services.utils.PPLCentralManagerOptionsBuilder;
import com.sclak.passepartout.utils.SclakGattAttributes;
import com.sclak.sportrick.R;
import com.sclak.sportrick.controllers.ProximityManager;
import com.sclak.sportrick.enums.RequestCode;

import java.util.Collection;

/**
 * Created by danielepoggi on 02/12/14.
 */
public class BluetoothActivity extends ActionbarActivity implements ICentralConsumer, RangeNotifier {

    private final static String TAG = BluetoothActivity.class.getSimpleName();
    private boolean skipBluetoothCheck = false;

    // bluetooth manager
    public PPLCentralManager2 centralManager = PPLCentralManager2.getInstanceForApplication(this, PPLCentralManager2.class);

    // btcode for which activate sclak
    private BluetoothResponseCallback currentCallback;
    private BleScannerNotifier scannerNotifier;

    // SINGLETON ACCESS
    private static BluetoothActivity INSTANCE;

    public BluetoothActivity getInstance() {
        return INSTANCE;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        INSTANCE = this;
        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // central manager options
        PPLCentralManagerOptions options = new PPLCentralManagerOptionsBuilder()
                .setDeviceIdentifiers("Sclak")
                .setBtcodeRoot("4CE2F1")
                .setAutoAuthenticate(true)
                .setAutoConnect(false)
                .setAutoScan(false)
                .setAutoOpenImmediate(true)     //auto open immediate mode !!!
                .setProximityImmediateM(0.01f)
                .setProximityNearM(5.0f)
                .setScanPeriod(1000) // millis
                .setShouldRetryConnection(true)
                .setShouldRetryAuthentication(true)
                .setShouldRetryCharacteristicWrite(true)
                .setBackgroundNotificationIconName("ic_sportrick")
                .setBackgroundNotificationPackageName(getApplicationContext().getPackageName())
                .setBackgroundNotificationTitle(getString(R.string.app_name))
                .build();

        if (!centralManager.initialize(options)) {
            Log.e(TAG, "Unable to initialize Bluetooth");
            return;
        }
        // can sclak authorization object is ApplicationController
        centralManager.authorizedSclakAction = A;

        //set auto open wait time
        centralManager.setAutoopenWaitTime(5000);          //5 seconds
    }

    @Override
    protected void onResume() {
        super.onResume();

        scannerNotifier = new BleScannerNotifier() {
            @Override
            public void didFindPeripheral(PPLDiscoveredPeripheral peripheral, boolean isNewPeripheral) {
                // update ProximityManger
                ProximityManager.getInstance().addProximityForBtcode(IBeacon.PROXIMITY_NEAR, peripheral.getBtcode());
            }

            @Override
            public void didLostPeripheral(PPLDiscoveredPeripheral peripheral) {
                // update ProximityManger
                ProximityManager.getInstance().addProximityForBtcode(IBeacon.PROXIMITY_UNKNOWN, peripheral.getBtcode());
            }

            @Override
            public void turnBluetoothOn() {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, RequestCode.EnableBluetooth.ordinal());
            }
        };
        centralManager.addBleScannerNotifier(scannerNotifier);

        //check if BT LE is available and turned ON
        try {
            if (centralManager.isBound(this)) {
                try {
                    centralManager.setBackgroundMode(BluetoothActivity.this, false);
                } catch (final Exception ex) {
                    Log.e(TAG, "Exception", ex);
                }
            } else {
                if (!skipBluetoothCheck) {
                    if (centralManager.checkAvailability()) {
                        centralManager.bind(this);
                    }
                }
            }
        } catch (BleNotAvailableException bex) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Application cannot be run without Bluetooth LE.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    }).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        centralManager.removeBleScannerNotifier(scannerNotifier);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        centralManager.unBind(this);
        centralManager.removeBleScannerNotifier(scannerNotifier);
        try {
            centralManager.stopScan();
        } catch (RemoteException e) {
            Log.e(TAG, e.getLocalizedMessage());
        }
        stopBluetooth();

        ProximityManager.getInstance().setProximityManagerListener(null);
    }

    // ICentralConsumer

    @Override
    public void onICentralServiceConnect() {
        updateManagerForeground();
    }

    // Activity Result

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == RequestCode.EnableBluetooth.ordinal()) {
                centralManager.bind(this);
            }
        } else if (resultCode == RESULT_CANCELED) {
            if (requestCode == RequestCode.EnableBluetooth.ordinal()) {
                //after first NO answer skip next BT check
                skipBluetoothCheck = true;
            }
        }
    }

    // BLUETOOTH manager

    public void startBluetooth() {
        //check if BT LE is available and turned ON
        try {
            if (centralManager.checkAvailability()) {
                centralManager.bind(this);
            }
        } catch (BleNotAvailableException bex) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Application cannot be run without Bluetooth LE.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    }).show();
        }
    }

    public void stopBluetooth() {
        centralManager.unBind(this);
    }

    public void sclakActionWithBtcode(final String btcode, final BluetoothResponseCallback callback) {
        Log.i(TAG, "sclakActionWithBtcode begin");
        if (null == btcode || btcode.length() == 0) {
            Log.e(TAG, "ILLEGAL ARGUMENT: btcode");
            return;
        }
        currentCallback = callback;

        final SclakPeripheral peripheral = centralManager.getOrRestorePeripheralWithBtcode(SclakPeripheral.class, btcode);
        if (null != peripheral) {
            // set secret for btcode if needed
            if (false == SecretManager.getInstance(this).hasSecretForBtcode(btcode)) {
                SecretManager.getInstance(this).setSecretForBtcode(btcode, A.BT_CODE_SECRET);
            }
            // check secret for btcode after having setted it
            if (false == SecretManager.getInstance(this).hasSecretForBtcode(btcode)) {
                // cannot continue!
                if (null != callback) {
                    callback.callback(false, new Exception("no secret for btcode"));
                }
                return;
            }

            peripheral.connectCallback(new BluetoothResponseCallback() {
                @Override
                public void callback(boolean success, Exception ex) {
                    if (!success) {
                        Toast.makeText(BluetoothActivity.this, R.string.sclak_failed_connection_error_message, Toast.LENGTH_LONG).show();
                        if (null != callback) {
                            callback.callback(false, new Exception("cannot connect"));
                        }
                    }
                }
            }, new BluetoothResponseCallback() {
                @Override
                public void callback(boolean success, Exception ex) {
                    if (success) {
                        int autoclose_time = 1;
                        peripheral.sclakCommandAutoCloseCallback((autoclose_time == 1 ? 10 : autoclose_time), new BluetoothResponseCallback() {
                            @Override
                            public void callback(boolean success, final Exception ex) {
                                if (success) {
                                    // disconnect immediately with proper authenticated command
                                    peripheral.disconnectCallback(new BluetoothResponseCallback() {
                                        @Override
                                        public void callback(boolean success, Exception ex) {
                                            Log.e(TAG, "peripheral disconnect result: " + success);
                                            // call callback
                                            if (null != currentCallback) {
                                                currentCallback.callback(success, ex);
                                            }
                                        }
                                    });
                                } else {
                                    Toast.makeText(BluetoothActivity.this, R.string.sclak_failed_sclak_action_error_message, Toast.LENGTH_LONG).show();
                                    if (null != callback) {
                                        callback.callback(false, new Exception("cannot sclak"));
                                    }
                                }
                            }
                        });
                    } else {
                        Toast.makeText(BluetoothActivity.this, R.string.sclak_failed_authentication_error_message, Toast.LENGTH_LONG).show();
                        // call callback
                        if (null != currentCallback) {
                            currentCallback.callback(false, ex);
                        }
                    }
                }
            }, new BluetoothResponseCallback() {
                @Override
                public void callback(boolean success, Exception ex) {
                    Log.d(TAG, "disconnect peripheral, desired: " + success);
                    if (!success) {
                        Toast.makeText(BluetoothActivity.this, R.string.sclak_failed_message, Toast.LENGTH_LONG).show();
                        if (null != callback) {
                            callback.callback(false, ex);
                        }
                    }
                }
            });

        } else {
            //Toast.makeText(this, R.string.sclak_failed_message, Toast.LENGTH_LONG).show();
            if (null != callback) {
                callback.callback(false, new Exception("peripheral is NULL"));
            }
        }
    }

    protected void updateManagerForeground() {
        if (!centralManager.isBound(this)) {
            return;
        }
        // create region
        Integer major = PPLBeacon.majorWithBtcode(A.BT_CODE_ENABLED);
        Integer minor = PPLBeacon.minorWithBtcode(A.BT_CODE_ENABLED);
        Region region = new Region(A.BT_CODE_ENABLED, SclakGattAttributes.SCLAK_PROXIMITY_UUID, major, minor);

        try {
            centralManager.setBackgroundMode(BluetoothActivity.this, false);
            centralManager.startScan();
            centralManager.setMonitorNotifier(null);
            centralManager.removeRangeNotifier(centralManager);
            centralManager.addRangeNotifier(this);
            centralManager.startRangingBeaconsInRegion(region);
        } catch (final Exception ex) {
            Log.e(TAG, "Exception", ex);
        }
    }

    protected void updateManagerBackground() {
        // create region
        Integer major = PPLBeacon.majorWithBtcode(A.BT_CODE_ENABLED);
        Integer minor = PPLBeacon.minorWithBtcode(A.BT_CODE_ENABLED);
        Region region = new Region(A.BT_CODE_ENABLED, SclakGattAttributes.SCLAK_PROXIMITY_UUID, major, minor);

        try {

            centralManager.setBackgroundMode(this, true);
            centralManager.setMonitorNotifier(centralManager);
            centralManager.startMonitoringBeaconsInRegion(region);
            centralManager.addRangeNotifier(centralManager);
        } catch (final Exception ex) {
            Log.e(TAG, "Exception", ex);
        }
    }

    @Override
    public void didRangeBeaconsInRegion(Collection<IBeacon> iBeacons, Region region) {
        for (IBeacon beacon : iBeacons) {
            String halfBtcode = PPLBeacon.halfBtcodeWithMajorMinor(beacon.getMajor(), beacon.getMinor());
            final String btcode = "4CE2F1" + halfBtcode;
            if (btcode.equals(A.BT_CODE_ENABLED)) {
                int beaconProximity = beacon.getProximity();
                // Log.d(TAG, "Proximity for: " + btcode + ", is " + beaconProximity + " (rssi: " + beacon.getRssi() + ")");
                ProximityManager.getInstance().addProximityForBtcode(beaconProximity, btcode);
            }
        }
    }

}
