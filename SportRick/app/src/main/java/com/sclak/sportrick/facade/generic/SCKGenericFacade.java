package com.sclak.sportrick.facade.generic;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import com.sclak.sportrick.SCKApplication;

import junit.framework.Assert;

import java.security.MessageDigest;

/**
 * Created by danielepoggi on 14/11/14.
 */
public class SCKGenericFacade {
    private static final String TAG = SCKGenericFacade.class.getSimpleName();

    // <editor-fold defaultstate="collapsed" desc="Context Management">

    private final boolean ENCRYPT_FILE = true;

    protected Context context;
    private static SCKApplication app;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    // <editor-fold defaultstate="collapsed" desc="Utils">

    protected Handler handler = new Handler();
    private String uniqueID = null;
    private final static String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
    public static final String PREFERENCES = "PREFERENCES";

    public SCKGenericFacade() {
        app = SCKApplication.getInstance();
    }

    /**
     * returns 32 bytes unique identifier
     *
     * @return
     */
    public String getUUID() {
        if (null != uniqueID) {
            return uniqueID;
        }
        if (null == context) {
            Log.e(TAG, "ILLEGAL STATE: context not setted.");
            Assert.assertNotNull(context);
            return null;
        }
        SharedPreferences sharedPrefs = app.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
        if (uniqueID == null) {
            /*
            // generation of random UUID that needs to be saved on PREFERENCES
            uniqueID = UUID.randomUUID().toString().replace("-", "").toUpperCase();
            */

            // get Secure.ANDROID_ID (64 bit, 8 byte)
            String androidID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID).toUpperCase();
            // but we need 16 bytes, so repeat androidId 2 times
            uniqueID = androidID + androidID;

            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString(PREF_UNIQUE_ID, uniqueID);
            editor.commit();
        }
        return uniqueID;
    }

    public String SHA1(String str) {
        String res = null;
        try {
            final MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] result = digest.digest(str.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (byte b : result)
                sb.append(String.format("%02X", b));
            res = sb.toString().toLowerCase();
            return res;
        } catch (final Exception ex) {
            Log.e("FACADE", "password sha1 exception: ", ex);
            return res;
        }
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model.toUpperCase();
        } else {
            return manufacturer.toUpperCase() + " " + model;
        }
    }

    // </editor-fold>

}
