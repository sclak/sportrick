package com.sclak.sportrick.controllers;

import android.content.Intent;
import android.util.Log;

import com.sclak.passepartout.services.ibeacon.IBeacon;
import com.sclak.sportrick.SCKApplication;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by danielepoggi on 03/12/14.
 */
public class ProximityManager {
    private static final String TAG = ProximityManager.class.getSimpleName();

    // NOTIFICATION
    public static final String PROXIMITY_CHANGED = "PROXIMITY_CHANGED";
    public static final String BTCODE_EXTRA = "BTCODE_EXTRA";
    public static final String PROXIMITY_EXTRA = "PROXIMITY_EXTRA";

    /**
     * limit of the timestamp array cache
     */
    private static final int CACHE_LIMIT = 10;                  // number of proximity samples stored in-memory

    /**
     * seconds required to unlock a change for a btcode
     * if the threshold have passed without collected timestamps, the btcode is away
     */
    private static final int TIMESTAMP_THRESHOLD = 7000;        // time needed to pass with # required samples for changing proximity - default: 10 seconds

    /**
     * number of equal kinds of proximity types, different from current one,
     * that needs to be detected before changing the current proximity type for a btcode
     */
    private static final int COUNTER_THRESHOLD = 1;             // number of samples needed for changing proximity - default: 1
    private static final int UNKNOWN_COUNTER_THRESHOLD = 2;     // number of unknown samples needed for changing proximity to unknown - default: 3

    private static ProximityManager INSTANCE;
    private ArrayList<Date> timestamps;
    private HashMap<String, Integer> proximities;

    // TIMER AND TASK
    private Timer timer;
    private HearthbeatTimerTask task;

    /**
     * manager primary cache
     * constains:
     * btcode --> map of timestamp / proximity type
     * collected with method addProximity
     */
    private HashMap<String, HashMap<Date, Integer>> cache;

    public interface IProximityManagerListener {
        public void proximityChanged(String btcode, Integer newProximity);
    }

    private IProximityManagerListener proximityManagerListener;

    public void setProximityManagerListener(IProximityManagerListener listener) {
        proximityManagerListener = listener;
    }

    private ProximityManager() {
        timestamps = new ArrayList<Date>();
        proximities = new HashMap<String, Integer>();
        cache = new HashMap<String, HashMap<Date, Integer>>();

        startHeartbeat();
    }

    public static ProximityManager getInstance() {
        if (null == INSTANCE) {
            INSTANCE = new ProximityManager();
        }
        return INSTANCE;
    }

    private void startHeartbeat() {
        if (null != timer) {
            timer.cancel();
            task.cancel();
        }
        timer = new Timer();
        task = new HearthbeatTimerTask();
        // delay 1000ms, repeat in 5000ms
        timer.schedule(task, 0, 1000);
    }

    public synchronized void addProximityForBtcode(final Integer proximity, final String btcode) {
        if (null == proximity) {
            Log.e(TAG, "ILLEGAL ARGUMENT: proximity");
            return;
        }
        if (null == btcode || btcode.length() == 0) {
            Log.e(TAG, "ILLEGAL ARGUMENT: btcode");
            return;
        }
        //Log.i(TAG, "add proximity " + proximity + " for btcode " + btcode);
        final Date timestamp = new Date();
        final HashMap<Date, Integer> record = new HashMap<Date, Integer>() {{
            put(timestamp, proximity);
        }};
        HashMap<Date, Integer> proximities = cache.get(btcode);
        if (null == proximities) {
            proximities = new HashMap<Date, Integer>();
            cache.put(btcode, proximities);
        }
        proximities.putAll(record);
        // clean cache by limiting timestamps to CACHE_LIMIT
        timestamps.add(timestamp);
        if (timestamps.size() > CACHE_LIMIT) {
            Date firstTimestamp = timestamps.get(0);
            proximities.remove(firstTimestamp);
            timestamps.remove(0);
        }
    }

    public Integer getProximityForBtcode(final String btcode) {
        Integer currentProximity = proximities.get(btcode);
        if (null == currentProximity) {
            return IBeacon.PROXIMITY_UNKNOWN;
        }
        return currentProximity;
    }

    private synchronized void updateProximities() {
        Date now = new Date();
        Set<String> keys = cache.keySet();
        for (String btcode : keys) {
            HashMap<Date, Integer> proximitiesCache = cache.get(btcode);
            int unknownCounter = 0;
            int nearCounter = 0;
            int farCounter = 0;
            int immediateCounter = 0;
            Set<Date> proximitiesKeys = proximitiesCache.keySet();
            for (Date timestamp : proximitiesKeys) {
                Integer proximity = proximitiesCache.get(timestamp);
                long timestampInterval = Math.abs(timestamp.getTime() - now.getTime());
                if (timestampInterval <= TIMESTAMP_THRESHOLD) {
                    switch (proximity) {
                        case IBeacon.PROXIMITY_UNKNOWN:
                            unknownCounter++;
                            break;
                        case IBeacon.PROXIMITY_IMMEDIATE:
                            immediateCounter++;
                            break;
                        case IBeacon.PROXIMITY_NEAR:
                            nearCounter++;
                            break;
                        case IBeacon.PROXIMITY_FAR:
                            farCounter++;
                            break;
                        default:
                            break;
                    }
                } else {
                    // unacceptable timestamp threshold, beacon is away
                    unknownCounter++;
                }
            }
            // detect proximity change
            Integer previousProximity = getProximityForBtcode(btcode);
            Integer newProximity = null;
            boolean changed = false;
            // update proximity
            if (immediateCounter >= COUNTER_THRESHOLD) {
                changed = previousProximity != IBeacon.PROXIMITY_IMMEDIATE;
                if (changed) {
                    newProximity = IBeacon.PROXIMITY_IMMEDIATE;
                }
            } else if (nearCounter >= COUNTER_THRESHOLD) {
                changed = previousProximity != IBeacon.PROXIMITY_NEAR;
                if (changed) {
                    newProximity = IBeacon.PROXIMITY_NEAR;
                }
            } else if (farCounter >= COUNTER_THRESHOLD) {
                changed = previousProximity != IBeacon.PROXIMITY_FAR;
                if (changed) {
                    newProximity = IBeacon.PROXIMITY_FAR;
                }
            } else if (unknownCounter >= UNKNOWN_COUNTER_THRESHOLD) {
                changed = previousProximity != IBeacon.PROXIMITY_UNKNOWN;
                if (changed) {
                    newProximity = IBeacon.PROXIMITY_UNKNOWN;
                }
            }
            // notification if needed
            if (changed) {
                proximities.put(btcode, newProximity);
                // reset proximity cache
                proximitiesCache.clear();
                // intent
                Intent intent = new Intent(PROXIMITY_CHANGED);
                intent.putExtra(BTCODE_EXTRA, btcode);
                intent.putExtra(PROXIMITY_EXTRA, newProximity);
                SCKApplication app = SCKApplication.getInstance();
                app.sendBroadcast(intent);
                // callback
                if (null != proximityManagerListener) {
                    proximityManagerListener.proximityChanged(btcode, newProximity);
                }
            }
        }
    }

    class HearthbeatTimerTask extends TimerTask {

        @Override
        public void run() {
            updateProximities();
        }
    }

    public static String getProximityName(Integer proximity) {
        switch (proximity) {
            case IBeacon.PROXIMITY_IMMEDIATE:
                return "Immediate";
            case IBeacon.PROXIMITY_NEAR:
                return "Near";
            case IBeacon.PROXIMITY_FAR:
                return "Far";
            case IBeacon.PROXIMITY_UNKNOWN:
                return "Unknown";
        }
        return "";
    }
}
